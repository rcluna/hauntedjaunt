﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lightning : MonoBehaviour
{
    public enum WaveForm {sin, tri, sqr, saw, inv, noise };
    public WaveForm waveform = WaveForm.sin;
    
    public float baseStart = 0.0f;
    public float amplitude = 1.0f;
    public float phase = 0.0f;
    public float frequency = 0.5f;

    private Color originalColor;
    public Color restingColor;
    private Light Light;

    
    float timerLength = 7.0f;
    float currentTimer;

    float lightningTimer = 2.0f;
    float currentLightning;
    // Start is called before the first frame update
    void Start()
    {
        Light = GetComponent<Light>();
        originalColor = Light.color;
        currentTimer = timerLength;
    }

    // Update is called once per frame
    void Update()
    {
        currentTimer -= Time.deltaTime;

        if (currentTimer <= 0)
        {
            currentTimer = timerLength;

            currentLightning = lightningTimer;
        }
        if (currentLightning > 0)
        {
            currentLightning -= Time.deltaTime;
            Light.color = originalColor * (EvalWave());
        }
        else
        {
            Light.color = restingColor;
        }    
    }
    float EvalWave()
    {
        float x = (Time.deltaTime + phase) * frequency;
        float y;
        x = x - Mathf.Floor(x);

        if (waveform == WaveForm.sin)
        {
            y = Mathf.Sin(x * 2 * Mathf.PI);
        }
        else if (waveform == WaveForm.tri)
        {
            if (x < 0.5f)
                y = 4.0f * x - 1.0f;
            else
                y = -4.0f * x + 3.0f;
    
        }
        else if  (waveform == WaveForm.saw)
        {
            y =x;
        }
        else if  (waveform == WaveForm.inv)
        {
            y= 1f -x;
        }
        else if  (waveform == WaveForm.noise)
        {
            y = 1.0f - (Random.value * 2);
        }
        else
        {
            y = 1.0f;
        }
        return (y * amplitude) + baseStart;
    
    }

}

